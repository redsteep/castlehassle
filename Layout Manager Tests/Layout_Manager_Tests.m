//
//  Layout_Manager_Tests.m
//  Layout Manager Tests
//
//  Created by Stanislav on 10.02.14.
//  Copyright (c) 2014 Itv. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "LayoutManager.h"

@interface Layout_Manager_Tests : XCTestCase

@end

@implementation Layout_Manager_Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

//- (void)testExample
//{
//    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
//}

- (void)testEmptyNodeScreenRelative
{
    CCNode *node = [[[CCNode alloc] init] autorelease];
    
    [node align:HorizontalCenter toScreen:Left];
    XCTAssertEqual(node.position.x, 0.0f);

    [node align:HorizontalCenter toScreen:HorizontalCenter];
    XCTAssertEqual(node.position.x, 240.0f);

    [node align:HorizontalCenter toScreen:Right];
    XCTAssertEqual(node.position.x, 480.0f);


    [node align:VerticalCenter toScreen:Bottom];
    XCTAssertEqual(node.position.y, 0.0f);

    [node align:VerticalCenter toScreen:VerticalCenter];
    XCTAssertEqual(node.position.y, 160.0f);

    [node align:VerticalCenter toScreen:Top];
    XCTAssertEqual(node.position.y, 320.0f);
}

- (void)testContentNodeHorizontalAlign {
    CCNode *node = [[[CCNode alloc] init] autorelease];
    node.contentSize = CGSizeMake(100, 50);
    
    CCNode *otherNode = [[[CCNode alloc] init] autorelease];
    otherNode.contentSize = CGSizeMake(200, 300);
    otherNode.position    = ccp(1000, 500);
    
    [node align:Left to:otherNode :Left];
    XCTAssertEqual(node.position.x, otherNode.position.x);

    [node align:Left to:otherNode :HorizontalCenter];
    XCTAssertEqual(node.position.x, otherNode.position.x + otherNode.contentSize.width/2);

    [node align:Left to:otherNode :Right];
    XCTAssertEqual(node.position.x, otherNode.position.x + otherNode.contentSize.width);

    
    [node align:HorizontalCenter to:otherNode :Left];
    XCTAssertEqual(node.position.x + node.contentSize.width/2, otherNode.position.x);
    
    [node align:HorizontalCenter to:otherNode :HorizontalCenter];
    XCTAssertEqual(node.position.x + node.contentSize.width/2, otherNode.position.x + otherNode.contentSize.width/2);
    
    [node align:HorizontalCenter to:otherNode :Right];
    XCTAssertEqual(node.position.x + node.contentSize.width/2, otherNode.position.x + otherNode.contentSize.width);

    
    [node align:Right to:otherNode :Left];
    XCTAssertEqual(node.position.x + node.contentSize.width, otherNode.position.x);
    
    [node align:Right to:otherNode :HorizontalCenter];
    XCTAssertEqual(node.position.x + node.contentSize.width, otherNode.position.x + otherNode.contentSize.width/2);
    
    [node align:Right to:otherNode :Right];
    XCTAssertEqual(node.position.x + node.contentSize.width, otherNode.position.x + otherNode.contentSize.width);
}

- (void)testContentNodeVerticalAlign {
    CCNode *node = [[[CCNode alloc] init] autorelease];
    node.contentSize = CGSizeMake(100, 50);
    
    CCNode *otherNode = [[[CCNode alloc] init] autorelease];
    otherNode.contentSize = CGSizeMake(200, 300);
    otherNode.position    = ccp(1000, 500);

    
    [node align:Bottom to:otherNode :Bottom];
    XCTAssertEqual(node.position.y, otherNode.position.y);
    
    [node align:Bottom to:otherNode :VerticalCenter];
    XCTAssertEqual(node.position.y, otherNode.position.y + otherNode.contentSize.height/2);
    
    [node align:Bottom to:otherNode :Top];
    XCTAssertEqual(node.position.y, otherNode.position.y + otherNode.contentSize.height);

    
    [node align:VerticalCenter to:otherNode :Bottom];
    XCTAssertEqual(node.position.y + node.contentSize.height/2, otherNode.position.y);
    
    [node align:VerticalCenter to:otherNode :VerticalCenter];
    XCTAssertEqual(node.position.y + node.contentSize.height/2, otherNode.position.y + otherNode.contentSize.height/2);
    
    [node align:VerticalCenter to:otherNode :Top];
    XCTAssertEqual(node.position.y + node.contentSize.height/2, otherNode.position.y + otherNode.contentSize.height);

    
    [node align:Top to:otherNode :Bottom];
    XCTAssertEqual(node.position.y + node.contentSize.height, otherNode.position.y);
    
    [node align:Top to:otherNode :VerticalCenter];
    XCTAssertEqual(node.position.y + node.contentSize.height, otherNode.position.y + otherNode.contentSize.height/2);
    
    [node align:Top to:otherNode :Top];
    XCTAssertEqual(node.position.y + node.contentSize.height, otherNode.position.y + otherNode.contentSize.height);
}


- (void)testContentNodeHorizontalAlignAnchorAtCenter {
    CCNode *node = [[[CCNode alloc] init] autorelease];
    node.contentSize = CGSizeMake(100, 50);
    node.anchorPoint = ccp(0.5, 0.5);
    
    CCNode *otherNode = [[[CCNode alloc] init] autorelease];
    otherNode.contentSize = CGSizeMake(200, 300);
    otherNode.position    = ccp(1000, 500);
    
    [node align:Left to:otherNode :Left];
    XCTAssertEqual(node.position.x - node.contentSize.width*node.anchorPoint.x, otherNode.position.x);
    
    [node align:Left to:otherNode :HorizontalCenter];
    XCTAssertEqual(node.position.x - node.contentSize.width*node.anchorPoint.x, otherNode.position.x + otherNode.contentSize.width/2);
    
    [node align:Left to:otherNode :Right];
    XCTAssertEqual(node.position.x - node.contentSize.width*node.anchorPoint.x, otherNode.position.x + otherNode.contentSize.width);
    
    
    [node align:HorizontalCenter to:otherNode :Left];
    XCTAssertEqual(node.position.x - node.contentSize.width*node.anchorPoint.x + node.contentSize.width/2, otherNode.position.x);
    
    [node align:HorizontalCenter to:otherNode :HorizontalCenter];
    XCTAssertEqual(node.position.x - node.contentSize.width*node.anchorPoint.x + node.contentSize.width/2, otherNode.position.x + otherNode.contentSize.width/2);
    
    [node align:HorizontalCenter to:otherNode :Right];
    XCTAssertEqual(node.position.x - node.contentSize.width*node.anchorPoint.x + node.contentSize.width/2, otherNode.position.x + otherNode.contentSize.width);
    
    
    [node align:Right to:otherNode :Left];
    XCTAssertEqual(node.position.x - node.contentSize.width*node.anchorPoint.x + node.contentSize.width, otherNode.position.x);
    
    [node align:Right to:otherNode :HorizontalCenter];
    XCTAssertEqual(node.position.x - node.contentSize.width*node.anchorPoint.x + node.contentSize.width, otherNode.position.x + otherNode.contentSize.width/2);
    
    [node align:Right to:otherNode :Right];
    XCTAssertEqual(node.position.x - node.contentSize.width*node.anchorPoint.x + node.contentSize.width, otherNode.position.x + otherNode.contentSize.width);
}

// Scene
// +- otherNode,  pos = (100, 75)
// +- parentNode, pos = (50, 25)
//    +- node     -- align to otherNode
//

- (void)testAlignChildNode {
    CCScene *scene = [[[CCScene alloc] init] autorelease];
    CCNode *parentNode = [[[CCNode alloc] init] autorelease];
    parentNode.position = ccp(50, 25);
    
    CCNode *node = [[[CCNode alloc] init] autorelease];
    node.contentSize = CGSizeMake(80, 40);
    
    CCNode *otherNode = [[[CCNode alloc] init] autorelease];
    otherNode.position = ccp(100, 75);
    
    [scene addChild:otherNode];
    [scene addChild:parentNode];
    [parentNode addChild:node];
    
    
    [node align:Left to:otherNode :Left];
    XCTAssertEqual(parentNode.position.x + node.position.x - node.contentSize.width*node.anchorPoint.x, otherNode.position.x);

    [node align:Left to:otherNode :Right];
    XCTAssertEqual(parentNode.position.x + node.position.x - node.contentSize.width*node.anchorPoint.x, otherNode.position.x + otherNode.contentSize.width);

}

- (void)testAlignToAnchoredNode {
    CCNode *node = [[[CCNode alloc] init] autorelease];
    node.contentSize = CGSizeMake(100, 50);
    node.anchorPoint = ccp(0.5, 0.5);
    
    CCNode *otherNode = [[[CCNode alloc] init] autorelease];
    otherNode.anchorPoint = ccp(0.5, 0.5);
    otherNode.contentSize = CGSizeMake(200, 300);
    otherNode.position    = ccp(1000, 500);
    
    
    [node align:Left to:otherNode :Left];
    XCTAssertEqual(node.position.x - node.contentSize.width*node.anchorPoint.x,
                   otherNode.position.x - otherNode.contentSize.width*otherNode.anchorPoint.x);
    
    [node align:Left to:otherNode :HorizontalCenter];
    XCTAssertEqual(node.position.x - node.contentSize.width*node.anchorPoint.x,
                   otherNode.position.x - otherNode.contentSize.width*otherNode.anchorPoint.x + otherNode.contentSize.width/2);
    
    [node align:Left to:otherNode :Right];
    XCTAssertEqual(node.position.x - node.contentSize.width*node.anchorPoint.x,
                   otherNode.position.x - otherNode.contentSize.width*otherNode.anchorPoint.x + otherNode.contentSize.width);
    
    
    [node align:HorizontalCenter to:otherNode :Left];
    XCTAssertEqual(node.position.x - node.contentSize.width*node.anchorPoint.x + node.contentSize.width/2,
                   otherNode.position.x - otherNode.contentSize.width*otherNode.anchorPoint.x);
    
    [node align:HorizontalCenter to:otherNode :HorizontalCenter];
    XCTAssertEqual(node.position.x - node.contentSize.width*node.anchorPoint.x + node.contentSize.width/2,
                   otherNode.position.x - otherNode.contentSize.width*otherNode.anchorPoint.x + otherNode.contentSize.width/2);
    
    [node align:HorizontalCenter to:otherNode :Right];
    XCTAssertEqual(node.position.x - node.contentSize.width*node.anchorPoint.x + node.contentSize.width/2,
                   otherNode.position.x - otherNode.contentSize.width*otherNode.anchorPoint.x + otherNode.contentSize.width);
    
    
    [node align:Right to:otherNode :Left];
    XCTAssertEqual(node.position.x - node.contentSize.width*node.anchorPoint.x + node.contentSize.width,
                   otherNode.position.x - otherNode.contentSize.width*otherNode.anchorPoint.x);
    
    [node align:Right to:otherNode :HorizontalCenter];
    XCTAssertEqual(node.position.x - node.contentSize.width*node.anchorPoint.x + node.contentSize.width,
                   otherNode.position.x - otherNode.contentSize.width*otherNode.anchorPoint.x + otherNode.contentSize.width/2);
    
    [node align:Right to:otherNode :Right];
    XCTAssertEqual(node.position.x - node.contentSize.width*node.anchorPoint.x + node.contentSize.width,
                   otherNode.position.x - otherNode.contentSize.width*otherNode.anchorPoint.x + otherNode.contentSize.width);
}

@end

