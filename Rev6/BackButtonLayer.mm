//
//  BackButtonLayer.mm
//  Rev5
//
//  Created by grady player on 10/30/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "BackButtonLayer.h"
#import "MainMenu.h"
#import "LayoutManager.h"


@implementation BackButtonLayer
-(id)init
{
	if ((self = [super init])) {
		CCNode *button = [self makeButtonWithString:NSLocalizedString(@"Back", @"Just a comment ...")
						atPosition:ccp(150,115) 
					  withSelector:@selector(back:)];
        [button align:TopBorder   toScreen:TopBorder margin:-20];
        [button align:RightBorder toScreen:RightBorder margin:-20];
	}
	return self;
}

-(void)back: (id) sender {
	MainMenu * main = [MainMenu instance];
	[main removeAllChildrenWithCleanup:YES];
	[main addChild:[MainMenuLayer node]];
}
@end
