//
//  CHToggle.m
//  Rev5
//
//  Created by Bryce Redd on 3/14/10.
//  Copyright 2010 Reel Connect LLC. All rights reserved.
//

#import "CHToggle.h"
#import "CHToggleItem.h"
#import "MainScene.h"


@implementation CHToggle

@synthesize image, selectedIndex;

-(id)initWithImageName:(NSString*)s {
	
	if( (self = [super init]) ) {
        self.image = s;
		items = [[NSMutableArray alloc] init];
        self.anchorPoint = ccp(0.5, 0.5);
        
//        CCLabelTTF *xLabel = [CCLabelTTF labelWithString:@"XXXXXXXXXXXXXXX" fontName:@"Arial" fontSize:12];
////        xLabel.position = difficulties.position;
//        xLabel.color = ccRED;
//        [self addChild:xLabel];
//
//        CCLabelTTF *oLabel = [CCLabelTTF labelWithString:@"O" fontName:@"Arial" fontSize:12];
////        xLabel.position = difficulties.position;
//        oLabel.color = ccBLACK;
//        [self addChild:oLabel];

//        self.contentSize = xLabel.contentSize;

	}
	return self;
}

//-(void)setPosition:(CGPoint)p {
//	position = p;
//	
//	for (uint i=0; i<[items count]; ++i) {
//		int offset = 0;
//		
//		if(i) {
//			offset += ((CHToggleItem*)[items objectAtIndex:i-1]).img.textureRect.size.width/2;
//			offset += ((CHToggleItem*)[items objectAtIndex:i-1]).img.position.x;
//			offset += ((CHToggleItem*)[items objectAtIndex:i]).img.textureRect.size.width/2;
//			offset -= 240;
//			[[items objectAtIndex:i] setPosition:ccp(offset, p.y)];
//		} else {
//			[[items objectAtIndex:i] setPosition:p];
//		}
//	}
//}

-(void)recalcItemsPositions {
    
    float currentX = 0;
    CGSize newContentSize = CGSizeZero;
    
    for (CHToggleItem *currentItem in items) {
        currentItem.position = ccp(currentX + currentItem.img.contentSize.width*currentItem.img.anchorPoint.x,
                                   self.contentSize.height/2);
        currentX += currentItem.img.contentSize.width;

        newContentSize.width  += currentItem.img.contentSize.width;
        newContentSize.height = MAX(newContentSize.height, currentItem.img.contentSize.height);
    }
    
    self.contentSize = newContentSize;
}

-(void)addItem:(CHToggleItem*)item {
	[items addObject:item];
    
    [self recalcItemsPositions];
    [self addChild:item];
    
	[self clearSelection];
	[self selectItemAtIndex:0];
}

-(void)selectItem:(id)sender {}

-(void)selectItemAtIndex:(int)index {
	[self clearSelection];
	CHToggleItem* item = [items objectAtIndex:index];
	[item setSelected:YES];
	selectedIndex = index;
}

-(void)clearSelection {
	for(CHToggleItem* item in items)
		[item setSelected:NO];
}

-(int)getSelected {
	return selectedIndex;
}

-(void)toggled:(id)sender {
	int index = [items indexOfObject:sender];
	selectedIndex = index;
	[self selectItemAtIndex:index];
	//[[Director sharedDirector] replaceScene: [MainScene scene]];
	
	// !!!!! [parent toggled:nil];
}

-(void) dealloc {
	[super dealloc];
	
//	[items release];
//    [image release];
}

@end
