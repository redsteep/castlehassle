//
//  CHToggleItem.m
//  Rev5
//
//  Created by Bryce Redd on 3/14/10.
//  Copyright 2010 Reel Connect LLC. All rights reserved.
//

#import "CHToggleItem.h"
#import "CHToggle.h"
#import "SinglePlayer.h"
#import "TVMacros.h"

@implementation CHToggleItem

@synthesize img, item, yOffset;

-(id) initWithParent:(CHToggle*)p selectedRect:(CGRect)sel deselectedRect:(CGRect)desel buttonText:(NSString*)str {
	
	if( (self =[super init]) ) {
		parent = p;
        
		selectedRect = sel;
		unselectedRect = desel;
        img = spriteWithRect(p.image, sel);
		[self addChild:img];
        
        self.contentSize = sel.size;
		
		item = [CCMenuItemFont itemFromString:str target:self selector:@selector(responder:)];
		
		item.anchorPoint = ccp(0.5, 0.5);
        item.contentSize = sel.size;
        item.label.position = ccp(self.contentSize.width/2, self.contentSize.height/2);
        item.label.anchorPoint = ccp(0.5, 0.5);
//        item.position = ccp(self.contentSize.width/2, self.contentSize.height/2);
		
		CCMenu* menu = [CCMenu menuWithItems:item, nil];
        menu.position = ccp(self.contentSize.width/2, self.contentSize.height/2);
        menu.contentSize = item.contentSize;
        menu.anchorPoint = ccp(0.5, 0.5);
        menu.isRelativeAnchorPoint = YES;
		
		[self addChild:menu];
	}
	
	return self;
	
}

-(void) setSelected:(BOOL)b {

	if(b) {
		img.textureRect = selectedRect;
		item.color = ccc3(240, 240, 240);
	} else {
		img.textureRect = unselectedRect;
		item.color = ccc3(30, 30, 30);
	}
	
}

//-(void) setPosition:(CGPoint)p {
//	img.position = ccpAdd(ccp(240.0,160.0), p);
//	item.position = ccpAdd(p, ccp(0, yOffset));	
//}

//- (void)setYOffset:(int)anYOffset {
//    yOffset = anYOffset;
//    [item setPosition:ccp(0, anYOffset)];
//}

-(void) responder:(id)sender {
	[parent toggled:self];
}

@end
