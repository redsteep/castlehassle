//
//  LayoutManager.h
//  Rev6
//
//  Created by Stanislav on 09.02.14.
//  Copyright (c) 2014 Itv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

typedef enum AlignType {
    LeftBorder, RightBorder, TopBorder, BottomBorder, HorizontalCenter, VerticalCenter
} AlignType;


@interface LayoutManager : NSObject

+ (LayoutManager *)sharedInstance;

- (void)alignNode:(CCNode *)node :(AlignType)nodeAlignType toScreen:(AlignType)screenAlignType;
- (void)alignNode:(CCNode *)node :(AlignType)nodeAlignType toScreen:(AlignType)screenAlignType margin:(float)margin;
- (void)alignNode:(CCNode *)node :(AlignType)nodeAlignType to:(CCNode *)otherNode :(AlignType)otherNodeAlignType;
- (void)alignNode:(CCNode *)node :(AlignType)nodeAlignType to:(CCNode *)otherNode :(AlignType)otherNodeAlignType margin:(float)margin;

@end


@interface CCNode (LayoutManagement)

- (void)align:(AlignType)nodeAlignType toScreen:(AlignType)screenAlignType;
- (void)align:(AlignType)nodeAlignType toScreen:(AlignType)screenAlignType margin:(float)margin;
- (void)align:(AlignType)nodeAlignType to:(CCNode *)otherNode :(AlignType)otherNodeAlignType;
- (void)align:(AlignType)nodeAlignType to:(CCNode *)otherNode :(AlignType)otherNodeAlignType margin:(float)margin;
- (void)fillParent;

@end