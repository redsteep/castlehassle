//
//  LayoutManager.m
//  Rev6
//
//  Created by Stanislav on 09.02.14.
//  Copyright (c) 2014 Itv. All rights reserved.
//

#import "LayoutManager.h"

@interface LayoutManager ()

- (CGRect)worldBoundingBox:(CCNode *)node;

@end


bool IsHorizontal(AlignType alignType) {
    return alignType == LeftBorder ||
           alignType == HorizontalCenter ||
           alignType == RightBorder;
}

@implementation LayoutManager

+ (LayoutManager *) sharedInstance {
    static LayoutManager *instance = nil;
    
    if (!instance) {
        instance = [[LayoutManager alloc] init];
    }
    
    return instance;
}

- (CGRect)worldBoundingBox:(CCNode *)node {
    CGRect boundingBoxInParent = node.boundingBox;
    
    CGPoint bottomLeft = boundingBoxInParent.origin;
    CGPoint topRight = ccpAdd(boundingBoxInParent.origin, ccp(boundingBoxInParent.size.width, boundingBoxInParent.size.height));
    
    CGPoint worldBottomLeft = [node.parent convertToWorldSpace:bottomLeft];
    CGPoint worldTopRight   = [node.parent convertToWorldSpace:topRight];
    
    return CGRectMake(worldBottomLeft.x, worldBottomLeft.y,
                      worldTopRight.x - worldBottomLeft.x, worldTopRight.y - worldBottomLeft.y);
}

- (void)alignNode:(CCNode *)node :(AlignType)nodeAlignType toScreen:(AlignType)screenAlignType {
    [self alignNode: node :nodeAlignType toScreen:screenAlignType margin:0];
}

- (void)alignNode:(CCNode *)node :(AlignType)nodeAlignType toScreen:(AlignType)screenAlignType margin:(float)margin {
    CCNode *screenNode = [[[CCNode alloc] init] autorelease];
    screenNode.contentSize = [CCDirector sharedDirector].winSize;
    screenNode.position = CGPointZero;
    [self alignNode:node :nodeAlignType to:screenNode :screenAlignType margin:margin];
}

- (void)alignNode:(CCNode *)node :(AlignType)nodeAlignType to:(CCNode *)otherNode :(AlignType)otherNodeAlignType {
    [self alignNode:node :nodeAlignType to:otherNode :otherNodeAlignType margin:0];
}

- (void)alignNode:(CCNode *)node :(AlignType)nodeAlignType to:(CCNode *)otherNode :(AlignType)otherNodeAlignType margin:(float)margin {
    CGPoint newPosition = node.position;
    
    if (IsHorizontal(nodeAlignType) != IsHorizontal(otherNodeAlignType)) {
        return;
    }
    
    switch (otherNodeAlignType) {
        case TopBorder:
            newPosition.y = otherNode.position.y + otherNode.contentSize.height * (1 - otherNode.anchorPoint.y);
            break;
        case VerticalCenter:
            newPosition.y = otherNode.position.y + otherNode.contentSize.height * (0.5 - otherNode.anchorPoint.y);
            break;
        case BottomBorder:
            newPosition.y = otherNode.position.y + otherNode.contentSize.height * (0.0 - otherNode.anchorPoint.y);
            break;
            
        case LeftBorder:
            newPosition.x = otherNode.position.x + otherNode.contentSize.width * (0.0 - otherNode.anchorPoint.x);
            break;
        case HorizontalCenter:
            newPosition.x = otherNode.position.x + otherNode.contentSize.width * (0.5 - otherNode.anchorPoint.x);
            break;
        case RightBorder:
            newPosition.x = otherNode.position.x + otherNode.contentSize.width * (1.0 - otherNode.anchorPoint.x);
            break;
    }
    
    if (otherNode.parent)
        newPosition = [otherNode.parent convertToWorldSpace:newPosition];
    
    if (node.parent)
        newPosition = [node.parent convertToNodeSpace:newPosition];
    
    switch (nodeAlignType) {
        case TopBorder:
            newPosition.y += node.contentSize.height*(node.anchorPoint.y - 1.0f);
            break;
        case VerticalCenter:
            newPosition.y += node.contentSize.height*(node.anchorPoint.y - 0.5f);
            break;
        case BottomBorder:
            newPosition.y += node.contentSize.height*node.anchorPoint.y;
            break;
            
        case LeftBorder:
            newPosition.x += node.contentSize.width*node.anchorPoint.x;
            break;
        case HorizontalCenter:
            newPosition.x += node.contentSize.width*(node.anchorPoint.x - 0.5f);
            break;
        case RightBorder:
            newPosition.x += node.contentSize.width*(node.anchorPoint.x - 1.0f);
            break;
    }
    
    if (IsHorizontal(nodeAlignType)) {
        newPosition.x += margin;
        newPosition.y = node.position.y;
    } else {
        newPosition.x = node.position.x;
        newPosition.y += margin;
    }
    
    node.position = newPosition;
}

@end



@implementation CCNode (LayoutManagement)

- (void)align:(AlignType)nodeAlignType toScreen:(AlignType)screenAlignType {
    [[LayoutManager sharedInstance] alignNode:self :nodeAlignType toScreen:screenAlignType];
}

- (void)align:(AlignType)nodeAlignType toScreen:(AlignType)screenAlignType margin:(float)margin {
    [[LayoutManager sharedInstance] alignNode:self :nodeAlignType toScreen:screenAlignType margin:margin];
}

- (void)align:(AlignType)nodeAlignType to:(CCNode *)otherNode :(AlignType)otherNodeAlignType {
    [[LayoutManager sharedInstance] alignNode:self :nodeAlignType to:otherNode :otherNodeAlignType];
}

- (void)align:(AlignType)nodeAlignType to:(CCNode *)otherNode :(AlignType)otherNodeAlignType margin:(float)margin {
    [[LayoutManager sharedInstance] alignNode:self :nodeAlignType to:otherNode :otherNodeAlignType margin:margin];
}

- (void)fillParent {
    if (!self.parent) return;
    
    float scale = MAX(self.parent.contentSize.width/self.contentSize.width,
                      self.parent.contentSize.height/self.contentSize.height);
    self.scale = scale;
    
//    [self align:HorizontalCenter to:self.parent :HorizontalCenter];
//    [self align:VerticalCenter   to:self.parent :VerticalCenter];
}

@end