//
//  SinglePlayer.m
//  Rev5
//
//  Created by Bryce Redd on 3/14/10.
//  Copyright 2010 Reel Connect LLC. All rights reserved.
//

#import "SinglePlayer.h"
#import "MainScene.h"
#import "CHToggle.h"
#import "CHToggleItem.h"
#import "MainMenu.h"
#import "MainScene.h"
#import "GameSettings.h"
#import "LayoutManager.h"

@implementation SinglePlayer

static SinglePlayer* instance = nil;

+(SinglePlayer *) instance {
	if(instance == nil) {
		instance = [SinglePlayer alloc];
		[SinglePlayer init];
	}
	
	return instance;
}

-(id) init {
	
	if( (self=[super init])) {
		
        CCSprite* bg = sprite(@"background.jpg");
//        [bg setPosition:ccp(240, 160)];
        [self addChild:bg z:0];
        
        [bg align:HorizontalCenter toScreen:HorizontalCenter];
        [bg align:VerticalCenter   toScreen:VerticalCenter];
        [bg fillParent];

		CCSprite* navBack = sprite(@"menuBack.png");
//        [navBack setPosition:ccp(240, 160)];
        [navBack align:HorizontalCenter toScreen:HorizontalCenter];
        [navBack align:VerticalCenter   toScreen:VerticalCenter];
		[self addChild:navBack z:0];
		
		CCLabelTTF* title = [CCLabelTTF labelWithString:@"Single Player Game" fontName:@"Arial-BoldMT" fontSize:24];
		[title setColor:ccc3(15, 147, 222)];
//		title.position = ccp(240,280);
        [title align:HorizontalCenter to:navBack :HorizontalCenter];
        [title align:TopBorder              to:navBack :TopBorder margin:-10];
		[self addChild:title z:1];//Pirla
				
		difficulties = [[CHToggle alloc] initWithImageName:@"comboButtons.png"];
		
        const CGRect LeftComboButtonSelectedRect = CGRectMake(0, 121, 94, 36);
        const CGRect LeftComboButtonDeselectedRect = CGRectMake(0, 157, 94, 36);
        const CGRect CenterComboButtonSelectedRect = CGRectMake(94, 121, 92, 36);
        const CGRect CenterComboButtonDeselectedRect = CGRectMake(94, 157, 92, 36);
        const CGRect RightComboButtonSelectedRect = CGRectMake(186, 121, 94, 36);
        const CGRect RightComboButtonDeselectedRect = CGRectMake(186, 157, 94, 36);
        
		CHToggleItem* easy = [[CHToggleItem alloc] initWithParent:difficulties 
													 selectedRect:LeftComboButtonSelectedRect
												   deselectedRect:LeftComboButtonDeselectedRect
													   buttonText:@"Easy"];
		[difficulties addItem:easy];
		[easy setYOffset:5];
		[easy release];
		
		CHToggleItem* medium = [[CHToggleItem alloc] initWithParent:difficulties 
													   selectedRect:CenterComboButtonSelectedRect
													 deselectedRect:CenterComboButtonDeselectedRect
														 buttonText:@"Medium"];
		[difficulties addItem:medium];
		[medium setYOffset:5];
		[medium release];
		
		CHToggleItem* hard = [[CHToggleItem alloc] initWithParent:difficulties
													 selectedRect:RightComboButtonSelectedRect
												   deselectedRect:RightComboButtonDeselectedRect
													   buttonText:@"Hard"];
		[difficulties addItem:hard];
		[hard setYOffset:5];
		[hard release];
		
//		[difficulties setPosition:ccp(-15,75)];
        [difficulties align:TopBorder to:navBack :TopBorder margin:-50];
        [difficulties align:RightBorder to:navBack :RightBorder margin:-10];
        
		[self addChild:difficulties z:3];
        
//        CCLabelTTF *xLabel = [CCLabelTTF labelWithString:@"X" fontName:@"Arial" fontSize:12];
//        xLabel.position = difficulties.position;
//        xLabel.color = ccBLACK;
//        [self addChild:xLabel];
        
		[toggles addObject:difficulties];
		[difficulties release];
		
		
		opponents = [[CHToggle alloc] initWithImageName:@"comboButtons.png"];
		
		CHToggleItem* one = [[CHToggleItem alloc] initWithParent:opponents 
													selectedRect:CGRectMake(0, 121, 94, 36) 
												  deselectedRect:CGRectMake(0, 157, 94, 36) 
													  buttonText:@"1"];
		[opponents addItem:one];
		[one setYOffset:5];
		[one release];
		
		CHToggleItem* two = [[CHToggleItem alloc] initWithParent:opponents 
													selectedRect:CGRectMake(94, 121, 92, 36) 
												  deselectedRect:CGRectMake(94, 157, 92, 36) 
													  buttonText:@"2"];
		[opponents addItem:two];
		[two setYOffset:5];
		[two release];
		
		CHToggleItem* three = [[CHToggleItem alloc] initWithParent:opponents 
													  selectedRect:CGRectMake(186, 121, 94, 36)
													deselectedRect:CGRectMake(186, 157, 94, 36) 
														buttonText:@"3"];
		[opponents addItem:three];
		[three setYOffset:5];
		[three release];
		
//		[opponents setPosition:ccp(-15,13)];
        [opponents align:TopBorder to:difficulties :BottomBorder margin:-30];
        [opponents align:RightBorder to:difficulties :RightBorder];
		[self addChild:opponents z:3];
		
		[toggles addObject:opponents];
		[opponents release];
		
		//Pirla start ****************************************
		
		environment = [[CHToggle alloc] initWithImageName:@"comboButtons.png"];
		
		CHToggleItem *tuscany = [[CHToggleItem alloc] initWithParent:environment 
													selectedRect:CGRectMake(0, 0, 94, 60) 
												  deselectedRect:CGRectMake(0, 61, 94, 60) 
													  buttonText:@"Tuscany"];
//        tuscany.item.label.position = ccp(tuscany.item.label.position.x, 10);
        
        [tuscany.item.label align:BottomBorder to:tuscany.item :BottomBorder margin:5];
		[environment addItem:tuscany];
		[tuscany release];
		
		CHToggleItem* saxony = [[CHToggleItem alloc] initWithParent:environment 
													selectedRect:CGRectMake(94, 0, 94, 60) 
												  deselectedRect:CGRectMake(94, 61, 94, 60) 
													  buttonText:@"Saxony"];
        [saxony.item.label align:BottomBorder to:saxony.item :BottomBorder margin:5];
		[environment addItem:saxony];
		[saxony release];
		
		CHToggleItem *brittany = [[CHToggleItem alloc] initWithParent:environment 
													  selectedRect:CGRectMake(188, 0, 94, 60)
													deselectedRect:CGRectMake(188, 61, 94, 60) 
														buttonText:@"Brittany"];
        [brittany.item.label align:BottomBorder to:brittany.item :BottomBorder margin:5];
		[environment addItem:brittany];
		[brittany release];
		
//		[environment setPosition:ccp(-15,-55)];
        [environment align:TopBorder to:opponents :BottomBorder margin:-30];
        [environment align:RightBorder to:difficulties :RightBorder];
		[self addChild:environment z:3];
		
		
		//DISPLAY LABELS in the Single Player Menu
		CCLabelTTF* difficultyLabel = [CCLabelTTF labelWithString:@"Difficulty"
                                                          fontName:@"Arial-BoldMT"
                                                          fontSize:18];
		[difficultyLabel setColor:ccc3(15, 147, 222)];
		[difficultyLabel setAnchorPoint:ccp(0,0)];
//		difficultyLabel.position = ccp(20,235);
        [difficultyLabel align:LeftBorder to:navBack :LeftBorder margin:10];
        [difficultyLabel align:VerticalCenter   to:difficulties :VerticalCenter];
		[self addChild:difficultyLabel];
		
		CCLabelTTF* opponentsLabel = [CCLabelTTF labelWithString:@"Opponents"
                                                         fontName:@"Arial-BoldMT"
                                                         fontSize:18];
		[opponentsLabel setColor:ccc3(15, 147, 222)];
		[opponentsLabel setAnchorPoint:ccp(0,0)];
//		opponentsLabel.position = ccp(20,175);
        [opponentsLabel align:LeftBorder to:navBack :LeftBorder margin:10];
        [opponentsLabel align:VerticalCenter   to:opponents :VerticalCenter];
		[self addChild:opponentsLabel];
		
		CCLabelTTF* environmentLabel = [CCLabelTTF labelWithString:@"Environment"
																		   fontName:@"Arial-BoldMT"
																		   fontSize:18];
		[environmentLabel setColor:ccc3(15, 147, 222)];
		[environmentLabel setAnchorPoint:ccp(0,0)];
//		environmentLabel.position = ccp(20,110);
        [environmentLabel align:LeftBorder to:navBack :LeftBorder margin:10];
//        [environmentLabel align:VerticalCenter   to:navBack :TopBorder margin:-150];
        [environmentLabel align:VerticalCenter   to:environment :VerticalCenter];
		[self addChild:environmentLabel];
		
        CCMenuItemSprite *button;
		button = [self makeButtonWithString:@"Start Game"
                                 atPosition:ccp(150,-120)
                               withSelector:@selector(startGame:)];
        [button align:RightBorder to:navBack :RightBorder margin:-10];
        [button align:BottomBorder to:navBack :BottomBorder margin:5];
        
		
		button = [self makeButtonWithString:@"Back"
                                 atPosition:ccp(-150,-120)
                               withSelector:@selector(previousScreen:)];
        [button align:LeftBorder to:navBack :LeftBorder margin:10];
        [button align:BottomBorder to:navBack :BottomBorder margin:5];
		
	}
	
	if(!instance)
		instance = self;
	
	return self;
}

-(void)startGame: (id)sender {
	GameSettings *gs = [GameSettings instance];
	[gs resetGameProperties];
	
	// get game data, 
	if([environment getSelected] == 0) 
		gs.backgroundType = tuscany;
	if([environment getSelected] == 1) 
		gs.backgroundType = saxony;
	if([environment getSelected] == 2) 
		gs.backgroundType = britany;
	
	gs.numPlayers = [opponents getSelected]+2;
	
	if([difficulties getSelected] == 0)
		gs.type = easy;
	if([difficulties getSelected] == 1)
		gs.type = medium;
	if([difficulties getSelected] == 2)
		gs.type = hard;
	
	[[CCDirector sharedDirector] replaceScene:[MainScene scene]];
}

-(void)previousScreen:(id)sender{
	MainMenu * main = [MainMenu instance];
	[main removeChild:self cleanup:YES];
	[main addChild:[MainMenuLayer node]];
}

-(void) dealloc {
	[toggles release];
	[super dealloc];
}

@end
